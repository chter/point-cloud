

### ref : https://medium.com/@somegreg/thinning-lidar-files-with-lastools-3ed92247fb0f

#### Thinning of LAS file (make smaller)
las2las -i input.las -keep_every_nth 2 -o output.las

#### Compression of LAS to LAZ (LAZ is 10% of LAS size)
las2las -i input.las -o output.laz
